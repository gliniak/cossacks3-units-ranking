"use strict";
wglb.prepareNs("cossacks3");

cossacks3.UnitUpgradeController = class {
    constructor(aUpgrades, aIsAttackUpgrades, aDivContainer, aCallback) {
        this._upgrades = aUpgrades.filter(this._isUpgradeCorrect);
        this._imagePrefix = aIsAttackUpgrades ? "images/upgrades/attack" : "images/upgrades/defence";
        this._itemNamer = aIsAttackUpgrades ? this._attackItemText.bind(this) : this._defenceItemText.bind(this);
        this._container = aDivContainer;
        this._callback = aCallback;

        this._create();
    }

    _create() {
        this._selectController = new cossacks3.SelectController(this._upgrades, "I", aUpgrade => {
            var index = this._upgrades.indexOf(aUpgrade);
            this._selectedUpgrades = this._upgrades.slice(0, index + 1);
            this._select.style.backgroundImage = "url('" + this._imagePrefix + (index + 2) + ".jpg')";
            this._callback(this._selectedUpgrades);
        }, this._itemNamer, true);

        this._select = this._selectController.selectItem;
        this._select.className = "Upgrade";
        this._container.appendChild(this._select);
    }

    _isUpgradeCorrect(aUpgrade) {
        const cost = aUpgrade.cost || {};
        const summaryCost = cost.food + cost.wood + cost.stone + cost.gold + cost.iron + cost.coal;
        return summaryCost > 0; // (at least) Dragoons VIIc have attack ups non-existing in game
    }

    _attackItemText(aUpgrade, aIndex) {
        var weapon = "";
        if (aUpgrade.pike) weapon = "pike";
        if (aUpgrade.sword) weapon = "sword";
        if (aUpgrade.bullet) weapon = "bullet";
        if (aUpgrade.arrow) weapon = "arrow";
        if (aUpgrade.mortarball) weapon = "mortarball";
        return weapon ? "\xA0\xA0\xA0\xA0\xA0\xA0Attack " + this._toRoman(aIndex+2) + " (" + weapon + " +" + aUpgrade[weapon] + ")" : "I";
    }

    _defenceItemText(aUpgrade, aIndex) {
        var bonus = aUpgrade.pike || aUpgrade.sword || aUpgrade.arrow;
        return bonus ? "\xA0\xA0\xA0\xA0\xA0\xA0Defence " + this._toRoman(aIndex+2) + " (+" + bonus + ")" : "I";
    }

    _toRoman(aNumber) {
        return ["0", "I", "II", "III", "IV", "V", "VI", "VII"][aNumber];
    }
};
