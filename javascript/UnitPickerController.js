"use strict";
wglb.prepareNs("cossacks3");

cossacks3.UnitPickerController = function(aUnitIndex, aContainer, aUnitPickedCallback) {
    this._unitIndex = aUnitIndex;
    this._container = aContainer;
    this._div = document.createElement("div");
    this._div.className = "PickerRow";
    this._nationSelect = null;
    this._nation = null;
    this._unitSelect = null;
    this._unitPickedCallback = aUnitPickedCallback;
    this._color = null;
    this._colorPicker = null;
    this._unitImage = null;
    this._upgradesDiv = null;
};

cossacks3.UnitPickerController.prototype.setNations = function(aNations) {
    this._colorPicker = new cossacks3.ColorPickerController(this._unitIndex, aColor => {
        gtag("event", "colorChanged", { event_label : aColor });
        this._sendCallback();
    });
    this._div.appendChild(this._colorPicker.selectItem);
    this._container.appendChild(this._div);

    var nationSelectController = new cossacks3.SelectController(aNations, "Select nation", aNation => {
        this._nationSelected(aNation);
    });
    this._nationSelect = nationSelectController.selectItem;
    this._div.appendChild(this._nationSelect);
};

cossacks3.UnitPickerController.prototype._nationSelected = function(aNation) {
    this._nation = aNation;
    var unitSelectController = new cossacks3.SelectController(aNation.units, "Select unit", this._unitSelected.bind(this));

    this._cleanUnitSelect();
    this._cleanUnitImage();
    this._cleanUpgrades();

    this._unitSelect = unitSelectController.selectItem;
    this._div.appendChild(this._unitSelect);
};

cossacks3.UnitPickerController.prototype._unitSelected = function(aUnit) {
    this._unit = aUnit;
    this._upgradesSum = cossacks3.createUpgradesSum();
    this._sendCallback();
    gtag("event", "unitPicked", { event_label : this._nation.id + "-" + aUnit.id });

    this._cleanUnitImage();
    this._cleanUpgrades();
    this._unitImage = cossacks3.createUnitImage(this._unit.id);
    this._div.appendChild(this._unitImage);

    var upgradesPicker = new cossacks3.UpgradesPicker(this._unit, this._nation, this._upgradesSum, aUpgrades => {
        this._upgradesSum = aUpgrades;
        this._sendCallback();
    });
    this._upgradesDiv = upgradesPicker.div;
    this._div.appendChild(this._upgradesDiv)
};

cossacks3.UnitPickerController.prototype._sendCallback = function() {
    if (this._unit && this._upgradesSum) {
        this._unitPickedCallback(this._unitIndex, new cossacks3.ExtendedUnit(this._unit, this._colorPicker.color, this._upgradesSum));
    }
};

cossacks3.UnitPickerController.prototype._cleanUnitSelect = function() {
    if (this._unitSelect) {
        this._div.removeChild(this._unitSelect);
    }
    this._unitSelect = null;
};

cossacks3.UnitPickerController.prototype._cleanUnitImage = function() {
    if (this._unitImage) {
        this._div.removeChild(this._unitImage);
    }
    this._unitImage = null;
};

cossacks3.UnitPickerController.prototype._cleanUpgrades = function() {
    if (this._upgradesDiv) {
        this._div.removeChild(this._upgradesDiv);
    }
    this._upgradesDiv = null;
};
