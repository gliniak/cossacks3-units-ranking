"use strict";
wglb.prepareNs("cossacks3");

cossacks3.Ranking = function() {
    this._unitPickersContainer = document.getElementById("unitPickerContainer");
    this._unitPickers = [];
    this._propertiesController = null;
    this._nations = null;
    this._units = [];
};

cossacks3.Ranking.prototype.start = function() {
    this._propertiesController = new cossacks3.PropertiesController();
    this._addUnitPicker();

    wglb.loadJSONAsync("data/generated2_upgrades.json", nationsJson => {
        this._nations = nationsJson.nations;
        this._setUpUnitPicker(0);
     });

     document.getElementById("addUnitButton").onclick = () => {
         this._addUnitPicker();
         gtag("event", "unitAdded");
     };
};

cossacks3.Ranking.prototype._setUpUnitPicker = function(aIndex) {
    this._unitPickers[aIndex].setNations(this._nations);
};

cossacks3.Ranking.prototype._addUnitPicker = function() {
    var index = this._unitPickers.length;
    var unitPicker = new cossacks3.UnitPickerController(index, this._unitPickersContainer, (aIndex, aUnitAndColor) => {
        console.log("picked:", aUnitAndColor);
        this._units[aIndex] = aUnitAndColor;
        this._propertiesController.setUnits(this._units);
    });
    this._unitPickers.push(unitPicker);

    if (index > 0) this._setUpUnitPicker(index);
};
