"use strict";

/**
 * wglb namespace object
 * Contains set of JS tools
 */
window.wglb = window.wglb || {};

/**
 * Creates namespace object if doesn't exist
 * @param {String} aNamespace
 */
wglb.prepareNs = function(aNamespace) {
    var array = aNamespace.split(".");
    var obj = window;
    for (var i = 0; i < array.length; ++i) {
        if (!obj[array[i]]) {
            obj[array[i]] = {};
        }
        obj = obj[array[i]];
    }
};

/**
 * Returns content of the file defined by url
 * Uses XmlHttpRequest synchronously
 * @param {String} url
 * @return {String} source text or null
 */
wglb.getContentSynch = function(url) {
    var req = new XMLHttpRequest();
    //TODO use that carefully
    req.overrideMimeType("text/plain");
    req.open("GET", url, false);
    req.send(null);
    return (req.status === 200 || req.status === 0) ? req.responseText : null;
};

/**
 * Returns content of the file defined by url
 * Uses XmlHttpRequest asynchronously
 * @param {String} url
 * @param {Function} callback - a function which will be called when file is loaded. The content of the file will be passes as a parameter of callback
 */
wglb.getContentAsynch = function(url, callback) {
    var req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.onreadystatechange = function (aEvt) {
        //console.debug(url + " " + req.readyState + " " + req.status)
        if (req.readyState === 4) {
            if (req.status === 200 || req.status === 0)
                callback(req.responseText);
            else
                throw new Error("wglb.getContentAsynch: couldn't open " + url + " code: " + req.status);
        }
    };
    req.send(null);
};

wglb.getContentAsyncPromise = function(url) {
    return new Promise((resolve, reject) => {
        var req = new XMLHttpRequest();

        req.open("GET", url, true);
        req.overrideMimeType("application/json");

        req.onreadystatechange = function (aEvt) {
            if (req.readyState === 4) {
                if (req.status === 200 || req.status === 0)
                    resolve(req.responseText);
                else
                    reject(new Error("wglb.getContentAsynch: couldn't open " + url + " code: " + req.status));
            }
        };
        req.send(null);
    });
};

/**
 * @param {String} url
 * @return {Object} the object stored in the JSON file
 */
wglb.loadJSON = function(url) { return JSON.parse(wglb.getContentSynch(url)); };

wglb.loadJSONAsync = function(url, callback) {
    wglb.getContentAsyncPromise(url)
    .then(aFileContent => JSON.parse(aFileContent))
    .then(json => { callback(json) })
    .catch(error => { console.error(error) })

};

(function() {
    var importedNs = [];
    /**
     * Imports namespace
     * @param {String} aNamespace
     */
    wglb.importNs = function(aNamespace) {
        if (!importedNs[aNamespace]) {
            try {
                aNamespace = aNamespace.replace(/\./g, "/") + ".js";
                var content = wglb.getContentSynch(aNamespace);
                if (content === null) {
                    wglb.error("Could not load " + aNamespace);
                } else {
                    window.eval(content);
                    importedNs[aNamespace] = true;
                }
            } catch (e) { wglb.error(e); }
        }
    };
})();

/**
 * Whether the browser is Firefox
 */
wglb.FF = /Firefox/.test(window.navigator.userAgent);

/**
 * Shows or hides DIV element
 * @param {HTMLElement} aDiv - the DIV to be shown / hidden
 * @param {Boolean} aShow
 */
wglb.showDiv = function(aDiv, aShow) {
    aDiv.setAttribute("style", aShow === false ? "display:none;" : "");
};

/**
 * Returns function with changed context (this reference) - there is Function.bind function
 */
wglb.bind = function (context, func, args) {
    return function () { func.apply(context, args); };
};

/**
 * Adds prototypal inheritance relation between "classes"
 * @param {Function} subClass
 * @param {Function} parentClass
 */
wglb.extend = function(subClass, parentClass) {
    var intermediate = function() {};
    intermediate.prototype = parentClass.prototype;
    subClass.prototype = new intermediate();
    subClass.prototype.superclass = parentClass;
    subClass.prototype.constructor = subClass;
};

//requestAnimationFrame fallback
//author: http://paulirish.com/2011/requestanimationframe-for-smart-animating/
if (!window.requestAnimationFrame) {
 window.requestAnimationFrame = (function() {
     return window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame ||
             window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
             function(callback, element) { window.setTimeout(callback, 1000 / 60); };
 })();
}

/**
 * Gets parameters from the current url
 * @returns {Object} in { prop1: value1, ..., propN: valueN }
 */
wglb.getUrlQuery = function() {
    var ret = {};
    var queryString = location.search.substring(1); // remove "?"
    var pairs = queryString.split("&");
    pairs.forEach(function(pair) {
        var item = pair.split("=");
        ret[item[0]] = decodeURIComponent(item[1]);
    });
    return ret;
};
