"use strict";
wglb.prepareNs("cossacks3");

cossacks3.ExtendedUnit = class {
    constructor(aUnit, aColor, aUpgradesSum) {
        this.unit = aUnit;
        this.color = aColor;
        this.upgrades = aUpgradesSum;
    }

    get buildtime() {
        return this.unit.buildtime * (1 + this.upgrades.buildtimePercent/100);
    }

    get numberOfUnits() {
        return Math.floor(cossacks3.buildTime / this.buildtime);
    }

    // cost

    summaryCost(aResource) {
        var unitsCount = this.numberOfUnits;
        var pricePercentageDrop = this.upgrades.pricepercentage;
        var cost = unitsCount * (this.unit.cost[aResource] * (1 - (pricePercentageDrop/100)));
        cost += (this.upgrades.cost[aResource] || 0);

        return cost;
    }

    // attack ranged

    get musketPause() {
        var basePause = this.unit.weapons.bullet.pause;
        var bulletPausePercent = this.upgrades.attack.bulletPausePercent;
        return basePause * bulletPausePercent;
    }

    get summaryShotsPowerPerSecond() {
        var weapon = this.unit.weapons.bullet || this.unit.weapons.arrow;
        if (!weapon) return 0;
        var unitsCount = this.numberOfUnits;
        const totalPause = this.unit.weapons.bullet ? this.musketPause : weapon.pause;
        return this.rangedUpgradedDamage * unitsCount / totalPause;
    }

    get rangedUpgradedDamage() {
        var weapons = this.unit.weapons;
        var rangedWeapon = weapons.bullet || weapons.arrow;
        if (!rangedWeapon) return 0;

        var rangedBaseDamage = rangedWeapon.damage;
        var rangedBonus = this.upgrades.attack.bullet || this.upgrades.attack.arrow;
        var rangedBonusPercent = this.upgrades.attack.bulletPercent;
        return (rangedBaseDamage + rangedBonus) * (1 + rangedBonusPercent/100);
    }

    get attackTypes() {
        return Object.keys(this.unit.weapons);
    }

    // attack melee

    get meleeAttackType() {
        let weapons = this.unit.weapons;
        if (weapons.sword) { return "sword" }
        if (weapons.pike) { return "pike" }
        return null;
    }

    get summaryMeleeAttack() {
        if (!this.unit.weapons.sword && !this.unit.weapons.pike) return 0;
        var unitsCount = this.numberOfUnits;
        return this.meleeUpgradedDamage * unitsCount;
    }

    get meleeUpgradedDamage() {
        var weapons = this.unit.weapons;
        if (!weapons.sword && !weapons.pike) return 0;

        var meleeBaseDamage = (weapons.sword || weapons.pike).damage;
        const attackUpgrades = this.upgrades.attack;
        if (weapons.sword) {
            var meleeBonus = attackUpgrades.sword
            var meleeBonusPercent = 0;
        } else {
            var meleeBonus = attackUpgrades.pike;
            var meleeBonusPercent = attackUpgrades.pikePercent;
        }

        return (meleeBaseDamage + meleeBonus) * (1 + meleeBonusPercent/100);
    }

    // defence

    upgradedDefence(aMeleeType) {
        let defence = this.unit.protection[aMeleeType] + this.upgrades.defence[aMeleeType];
        let shield = this.unit.shield + this.upgrades.shield;
        return defence + shield;
    }
}
