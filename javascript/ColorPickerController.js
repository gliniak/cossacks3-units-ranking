"use strict";
wglb.prepareNs("cossacks3");

cossacks3.ColorPickerController = function(aIndex, aCallback) {
    this.selectItem = document.createElement("select");
    this.selectItem.className = "ColorPicker";

    var colors = ['#ce0807', '#1f65cb', '#3affa0', '#923dd8', '#ff6a06', '#303030', '#a4ffff', '#904407', '#ffffff',
                    '#24b412', '#ff508c', '#ffeb00', '#0099a7', '#8cbe50'];

    var innerHTML = "";
    for (let color of colors) {
        innerHTML += "<option value='" + color + "' style='background-color: " + color + ";'>&nbsp</option>";
    }
    this.selectItem.innerHTML = innerHTML;

    this.selectItem.addEventListener("change", aEvent => {
        this.selectItem.style.background = cossacks3.flag(aEvent.target.value);
        this.selectItem.style.backgroundSize = "50%";
        this.color = aEvent.target.value;
        aCallback(this.color);
    });

    var index = aIndex % colors.length;
    this.selectItem.options.selectedIndex = index;
    this.selectItem.style.background = cossacks3.flag(colors[index]);
    this.selectItem.style.backgroundSize = "50%";
    this.color = colors[index];
};
