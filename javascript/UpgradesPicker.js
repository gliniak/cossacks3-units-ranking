"use strict";
wglb.prepareNs("cossacks3");

cossacks3.UpgradesPicker = function(aUnit, aNation, aUpgradesSum, aUpgradesChangedCallback) {
    this._unit = aUnit;
    this._attackUpgrades = (aUnit.upgrades || {}).attack;
    this._defenceUpgrades = (aUnit.upgrades || {}).defence;
    this._selectedAttackUpgrades = [];
    this._selectedDefenceUpgrades = [];
    this._attackController = null;
    this._defenceController = null;
    this._blacksmithUpgrades = aNation.blacksmithUpgrades;
    this._blacksmithControllers = [];
    this._academyUpgrades = aNation.academyUpgrades;
    this._academyControllers = [];
    this._callback = aUpgradesChangedCallback;
    this._upgradesSum = aUpgradesSum;
    this._init();
};

cossacks3.UpgradesPicker.prototype._init = function() {
    var div = document.createElement("div");
    div.className = "Upgrades";

    if (this._attackUpgrades) {
        this._attackUpgrades.forEach(element => { element.type = "damage"; });

        this._attackController = new cossacks3.UnitUpgradeController(this._attackUpgrades, true, div, aUpgrades => {
            this._upgradesSum.removeUpgrades(this._selectedAttackUpgrades);

            this._selectedAttackUpgrades = aUpgrades;
            this._upgradesSum.applyUpgrades(this._selectedAttackUpgrades);
            this._callback(this._upgradesSum);
        });
    }

    if (this._defenceUpgrades) {
        this._defenceUpgrades.forEach(element => { element.type = "defence"; });
        this._attackController = new cossacks3.UnitUpgradeController(this._defenceUpgrades, false, div, aUpgrades => {
            this._upgradesSum.removeUpgrades(this._selectedDefenceUpgrades);

            this._selectedDefenceUpgrades = aUpgrades;
            this._upgradesSum.applyUpgrades(this._selectedDefenceUpgrades);
            this._callback(this._upgradesSum);
        });
        div.appendChild(this._createSeparator())
    }

    if (this._blacksmithUpgrades) {
        this._blacksmithUpgrades.forEach((upgrade, idx) => {
            if (!upgrade.affectedUnits.includes(this._unit.id)) return;

            var upgradeController = new cossacks3.CountryUpgradeController(upgrade, upgrade.id.substring(3) + ".jpg", this._upgradesSum, () => {
                this._callback(this._upgradesSum);
            });
            this._blacksmithControllers.push(upgradeController);
            div.appendChild(upgradeController.createElement());
        });
        div.appendChild(this._createSeparator())
    }

    if (this._academyUpgrades) {
        this._academyUpgrades.forEach((upgrade, idx) => {
            if (!upgrade.affectedUnits.includes(this._unit.id)) return;

            var upgradeController = new cossacks3.CountryUpgradeController(upgrade, upgrade.id.substring(3) + ".jpg", this._upgradesSum, () => {
                this._callback(this._upgradesSum);
            });
            this._academyControllers.push(upgradeController);
            div.appendChild(upgradeController.createElement());
        });
    }

    this.div = div;
};

cossacks3.UpgradesPicker.prototype._createSeparator = function() {
    var div = document.createElement("div");
    div.className = "UpgradesSeparator";
    return div;
};
