"use strict";
wglb.prepareNs("cossacks3");

cossacks3.buildTime = 10 * 60;

cossacks3.PropertiesController = function() {
    this._propertiesDiv = document.getElementById("properties");
    this.hpDiv = document.getElementById("maxhp");
    this.hpBuildTimeDiv = document.getElementById("buildtime");

    this._numberOfHitsTd = document.getElementById("numberOfHits");
    this._costFoodTd = document.getElementById("costFood");
    this._costWoodTd = document.getElementById("costWood");
    this._costStoneTd = document.getElementById("costStone");
    this._costGoldTd = document.getElementById("costGold");
    this._costIronTd = document.getElementById("costIron");
    this._costCoalTd = document.getElementById("costCoal");

    this._costFoodTotalTd = document.getElementById("costFoodTotal");
    this._costWoodTotalTd = document.getElementById("costWoodTotal");
    this._costStoneTotalTd = document.getElementById("costStoneTotal");
    this._costGoldTotalTd = document.getElementById("costGoldTotal");
    this._costIronTotalTd = document.getElementById("costIronTotal");
    this._costCoalTotalTd = document.getElementById("costCoalTotal");

    this._shieldTd = document.getElementById("shield");
    this._protectionSwordTd = document.getElementById("protectionSword");
    this._protectionPikeTd = document.getElementById("protectionPike");
    this._protectionBulletTd = document.getElementById("protectionBullet");
    this._protectionArrowTd = document.getElementById("protectionArrow");
    this._protectionCannisterTd = document.getElementById("protectionCannister");
    this._protectionCannonballTd = document.getElementById("protectionCannonball");

    this._damageMeleeTd = document.getElementById("damageMelee");
    this._damageRangedTd = document.getElementById("damageRanged");
    this._pauseRangedTd = document.getElementById("pauseRanged");
    this._rangeRangedTd = document.getElementById("rangeRanged");

    this._unitsCountTd = document.getElementById("unitsCount");
    this._shotPowerTd = document.getElementById("shotPower");
    this._meleePowerTd = document.getElementById("meleePower");
};

cossacks3.PropertiesController.prototype.setUnits = function(aUnitsAndColors) {
    var unitsAndColors = aUnitsAndColors.filter(unit => unit !== undefined);
    this._clearAll();

    this._setMaxHp(unitsAndColors);
    this._setBuildTime(unitsAndColors);
    this._setDirectFight(unitsAndColors);
    this._setCost(unitsAndColors);
    this._setProtection(unitsAndColors);
    this._setAttack(unitsAndColors);
    this._setUnitsCount(unitsAndColors);
    this._setShotPower(unitsAndColors);
    this._setMeleePower(unitsAndColors);
    this._propertiesDiv.style.visibility = "visible";
};

cossacks3.PropertiesController.prototype._clearAll = function() {
    this.hpDiv.innerHTML = "";
    this.hpBuildTimeDiv.innerHTML = "";

    this._numberOfHitsTd.innerHTML = "";
    this._costFoodTd.innerHTML = "";
    this._costWoodTd.innerHTML = "";
    this._costStoneTd.innerHTML = "";
    this._costGoldTd.innerHTML = "";
    this._costIronTd.innerHTML = "";
    this._costCoalTd.innerHTML = "";
    this._costFoodTotalTd.innerHTML = "";
    this._costWoodTotalTd.innerHTML = "";
    this._costStoneTotalTd.innerHTML = "";
    this._costGoldTotalTd.innerHTML = "";
    this._costIronTotalTd.innerHTML = "";
    this._costCoalTotalTd.innerHTML = "";

    this._shieldTd.innerHTML = "";
    this._protectionSwordTd.innerHTML = "";
    this._protectionPikeTd.innerHTML = "";
    this._protectionBulletTd.innerHTML = "";
    this._protectionArrowTd.innerHTML = "";
    this._protectionCannisterTd.innerHTML = "";
    this._protectionCannonballTd.innerHTML = "";

    this._damageMeleeTd.innerHTML = "";
    this._damageRangedTd.innerHTML = "";
    this._pauseRangedTd.innerHTML = "";
    this._rangeRangedTd.innerHTML = "";

    this._unitsCountTd.innerHTML = "";
    this._shotPowerTd.innerHTML = "";
    this._meleePowerTd.innerHTML = "";
};

cossacks3.PropertiesController.prototype._setMaxHp = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        this.hpDiv.appendChild(cossacks3.createBar(unitAndColor.unit.maxhp, unitAndColor.color));
    }
};

cossacks3.PropertiesController.prototype._setBuildTime = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        this.hpBuildTimeDiv.appendChild(cossacks3.createBar(unitAndColor.buildtime, unitAndColor.color));
    }
};

cossacks3.PropertiesController.prototype._setDirectFight = function(aUnitsAndColors) {
    let unit1 = aUnitsAndColors[0];
    let unit2 = aUnitsAndColors[1];
    if (!unit1 || !unit2) { return; }

    const onlyNormalWeapon = (weapon => ["pike", "sword", "bullet", "arrow"].includes(weapon));
    let unit1AttackTypes = unit1.attackTypes.filter(onlyNormalWeapon);
    let unit2AttackTypes = unit2.attackTypes.filter(onlyNormalWeapon);

    unit2AttackTypes.forEach(weapon => {
        this._addHitsUntilDeathBar(unit1, unit2, weapon);
    });
    unit1AttackTypes.forEach(weapon => {
        this._addHitsUntilDeathBar(unit2, unit1, weapon);
    });
};

cossacks3.PropertiesController.prototype._addHitsUntilDeathBar = function(aUnit1, aUnit2, aUnit2Weapon) {
    let unit1Defence = aUnit1.upgradedDefence(aUnit2Weapon);
    const isMeleeAttack = (aUnit2Weapon === "pike") || (aUnit2Weapon === "sword");
    let unit2Attack = isMeleeAttack ? aUnit2.meleeUpgradedDamage : aUnit2.rangedUpgradedDamage;

    let minHit = 1;

    let unit2EffectiveAttack = Math.max(unit2Attack - unit1Defence, minHit);
    let unit1HitsUntilDeath = Math.floor(aUnit1.unit.maxhp / unit2EffectiveAttack);

    const image = this._imagePathForConcreteWeapon(aUnit2Weapon);
    this._numberOfHitsTd.appendChild(cossacks3.createBar(unit1HitsUntilDeath, aUnit1.color, image));
};

cossacks3.PropertiesController.prototype._setCost = function(aUnitsAndColors) {
    var createBar = cossacks3.createBar;
    for (let unitAndColor of aUnitsAndColors) {
        var color = unitAndColor.color;
        var cost = unitAndColor.unit.cost;
        const priceDrop = 1 - unitAndColor.upgrades.pricepercentage/100;
        this._costFoodTd.appendChild(createBar(cost.food * priceDrop, color));
        this._costWoodTd.appendChild(createBar(cost.wood * priceDrop, color));
        this._costStoneTd.appendChild(createBar(cost.stone * priceDrop, color));
        this._costGoldTd.appendChild(createBar(cost.gold * priceDrop, color));
        this._costIronTd.appendChild(createBar(cost.iron * priceDrop, color));
        this._costCoalTd.appendChild(createBar(cost.coal * priceDrop, color));

        var barScale = 0.001;
        var totalFoodCost = unitAndColor.summaryCost("food");
        this._costFoodTotalTd.appendChild(createBar(totalFoodCost, color, barScale));
        var totalGoldCost = unitAndColor.summaryCost("gold");
        this._costGoldTotalTd.appendChild(createBar(totalGoldCost, color, barScale));
        var totalStoneCost = unitAndColor.summaryCost("stone");
        this._costStoneTotalTd.appendChild(createBar(totalStoneCost, color, barScale));
        var totalWoodCost = unitAndColor.summaryCost("wood");
        this._costWoodTotalTd.appendChild(createBar(totalWoodCost, color, barScale));
        var totalIronCost = unitAndColor.summaryCost("iron");
        this._costIronTotalTd.appendChild(createBar(totalIronCost, color, barScale));
        var totalCoalCost = unitAndColor.summaryCost("coal");
        this._costCoalTotalTd.appendChild(createBar(totalCoalCost, color, barScale));
    }
};

cossacks3.PropertiesController.prototype._setProtection = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        var color = unitAndColor.color;

        this._shieldTd.appendChild(cossacks3.createBar(unitAndColor.unit.shield + unitAndColor.upgrades.shield, color));

        var protection = unitAndColor.unit.protection;
        var defenceBonuses = unitAndColor.upgrades.defence;
        var swordBonus = defenceBonuses.sword;
        this._protectionSwordTd.appendChild(cossacks3.createBar([protection.sword, swordBonus], color));
        var pikeBonus = defenceBonuses.pike;
        this._protectionPikeTd.appendChild(cossacks3.createBar([protection.pike, pikeBonus], color));
        var bulletBonus = defenceBonuses.bullet;
        this._protectionBulletTd.appendChild(cossacks3.createBar([protection.bullet, bulletBonus], color));//just in case - there's no bullet bonus AFAIK
        var arrowBonus = defenceBonuses.arrow;
        this._protectionArrowTd.appendChild(cossacks3.createBar([protection.arrow, arrowBonus], color));
        this._protectionCannisterTd.appendChild(cossacks3.createBar(protection.cannister, color));
        this._protectionCannonballTd.appendChild(cossacks3.createBar(protection.cannonball, color));
    }
};

cossacks3.PropertiesController.prototype._setAttack = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        this._setRangedAttack(unitAndColor);
        this._setMeleeAttack(unitAndColor);
    }
};

cossacks3.PropertiesController.prototype._setMeleeAttack = function(aUnitAndColor) {
    var color = aUnitAndColor.color;
    var weapons = aUnitAndColor.unit.weapons;

    var meleeWeapon = weapons.sword || weapons.pike;
    var meleeBaseDamage = 0;
    var meleeUpgradedDamage = 0;
    if (meleeWeapon) {
        meleeBaseDamage = meleeWeapon.damage;
        meleeUpgradedDamage = aUnitAndColor.meleeUpgradedDamage;
    }
    this._damageMeleeTd.appendChild(meleeWeapon ? cossacks3.createBar([meleeBaseDamage, meleeUpgradedDamage - meleeBaseDamage], color, this._imagePathForWeapon("melee", weapons)) : this._noneDiv());
};

cossacks3.PropertiesController.prototype._setRangedAttack = function(aUnitAndColor) {
    var color = aUnitAndColor.color;
    var weapons = aUnitAndColor.unit.weapons;

    var rangedWeapon = weapons.bullet || weapons.arrow;
    if (!rangedWeapon) {
        this._damageRangedTd.appendChild(this._noneDiv());
        this._pauseRangedTd.appendChild(this._noneDiv());
        this._rangeRangedTd.appendChild(this._noneDiv());
        return;
    }
    var rangedBaseDamage = rangedWeapon.damage;
    var rangedUpgradedDamage = aUnitAndColor.rangedUpgradedDamage;
    this._damageRangedTd.appendChild(cossacks3.createBar([rangedBaseDamage, rangedUpgradedDamage - rangedBaseDamage], color, this._imagePathForWeapon("ranged", weapons)));
    const upgradedPause = weapons.bullet ? aUnitAndColor.musketPause : rangedWeapon.pause;
    this._pauseRangedTd.appendChild(cossacks3.createBar(upgradedPause, color));
    this._rangeRangedTd.appendChild(cossacks3.createRangeBar(rangedWeapon.radiusmin, rangedWeapon.radiusmax, color, 10));
}

cossacks3.PropertiesController.prototype._setUnitsCount = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        var color = unitAndColor.color;
        this._unitsCountTd.appendChild(cossacks3.createBar(unitAndColor.numberOfUnits, color));
    }
};

cossacks3.PropertiesController.prototype._setShotPower = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        var color = unitAndColor.color;
        this._shotPowerTd.appendChild(cossacks3.createBar(unitAndColor.summaryShotsPowerPerSecond, color, 0.05));
    }
};

cossacks3.PropertiesController.prototype._setMeleePower = function(aUnitsAndColors) {
    for (let unitAndColor of aUnitsAndColors) {
        var color = unitAndColor.color;
        this._meleePowerTd.appendChild(cossacks3.createBar(unitAndColor.summaryMeleeAttack, color, 0.05));
    }
};

cossacks3.PropertiesController.prototype._noneDiv = function() {
    var div = document.createElement("div");
    div.innerHTML = "-";
    return div;
};

cossacks3.PropertiesController.prototype._imagePathForWeapon = function(aKind, aWeapons) {
    var prefix = "images/properties/attack_";
    if (aKind === "melee") {
        return aWeapons.sword ? (prefix + "sword.png") : (aWeapons.pike ? (prefix + "pike.png") : null);
    } else if (aKind === "ranged") {
        return aWeapons.bullet ? (prefix + "muskeet.png") : (aWeapons.arrow ? (prefix + "arrow.png") : null);
    }
    return null;
};

cossacks3.PropertiesController.prototype._imagePathForConcreteWeapon = function(aWeapon) {
    if (aWeapon === "bullet") { aWeapon = "muskeet"; }
    return "images/properties/attack_" + aWeapon + ".png";
};