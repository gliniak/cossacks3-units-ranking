"use strict";
wglb.prepareNs("cossacks3");

cossacks3.CountryUpgradeController = class {
    constructor(aUpgrade, aImageUrl, aUpgradesSum, aCallback) {
        this._upgrade = aUpgrade;
        this._imageUrl = "images/upgrades/" + aImageUrl;
        this._enabled = false;
        this._upgradesSum = aUpgradesSum;
        this._callback = aCallback;
    }

    createElement() {
        var img = document.createElement("img");
        img.src = this._imageUrl;
        img.className = this._enabled ? "Upgrade Selected" : "Upgrade";
        img.onclick = () => {
            this._enabled = !this._enabled;
            img.className = this._enabled ? "Upgrade Selected" : "Upgrade";
            this._updateSum();
            this._callback();
        };
        return img;
    }

    _updateSum() {
        if (this._enabled) {
            this._upgradesSum.applyUpgrade(this._upgrade);
        } else {
            this._upgradesSum.removeUpgrade(this._upgrade);
        }
    }
}
