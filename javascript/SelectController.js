"use strict";
wglb.prepareNs("cossacks3");

cossacks3.SelectController = function(aItems, aTitle, aCallback, aItemTitle, aCallbackForFirst) {
    this._items = aItems;
    this._callbackForFirst = aCallbackForFirst === undefined ? false : aCallbackForFirst;
    this._itemTitle = aItemTitle || (aItem => (aItem.name + (aItem.id ? " (" + aItem.id + ")" : "")));
    this.selectItem = this._createSelect(aTitle);

    for (var i = 0; i < aItems.length; ++i) {
        var item = aItems[i];
        var option = this._createOption(item, i);
        this.selectItem.appendChild(option);
    }

    this.selectItem.addEventListener("change", aEvent => {
        var index = aEvent.target.value;
        if (index == -1) {
            if (this._callbackForFirst) {
                aCallback(null);
            }
            return;
        }

        var selectedItem = this._items[index];
        aCallback(selectedItem);
    });
};

cossacks3.SelectController.prototype._createSelect = function(aTitle) {
    var select = document.createElement("select");
    select.appendChild(this._createOption({name:aTitle}, -1));

    return select;
};

cossacks3.SelectController.prototype._createOption = function(aItem, aIndex) {
    var el = document.createElement("option");
    el.textContent = this._itemTitle(aItem, aIndex);
    el.value = aIndex;
    return el;
};
