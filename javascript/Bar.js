"use strict";
wglb.prepareNs("cossacks3");

cossacks3.createBar = function(aValue, aColor, aImageOrScale) {
    var scale = (typeof aImageOrScale === "number") ? aImageOrScale : 1;
    var bonusDivTemplate = "";
    var bonus = 0;
    if (aValue.constructor === Array) {
        bonus = aValue[1];
        aValue = aValue[0];
        bonusDivTemplate = bonus ? "<div class='Bar' " +
                                        "style='width: " + (bonus * scale) + "px; " + cossacks3._barColorStyle(aColor) + cossacks3._barBorderStyle(aColor) + "'>" +
                                    "</div>" : "";
    }

    aValue = Math.round(aValue * 100) / 100;
    bonus = Math.round(bonus * 100) / 100;
    var image = (typeof aImageOrScale === "string") ? this._createBarImage(aImageOrScale) : "";
    var div = document.createElement("div");
    var template = "<div class='Bar' " +
                        "style='width: " + (aValue * scale) + "px; " + cossacks3._barColorStyle(aColor) + cossacks3._barBorderStyle(aColor) + "'>" +
                    "</div>" +
                    bonusDivTemplate +
                    "<div class='BarValue'>" + (aValue + bonus) + "</div>" +
                    image;
    div.innerHTML = template;
    return div;
};

cossacks3.createRangeBar = function(aMinValue, aMaxValue, aColor, aScale) {
    aScale = aScale || 1;
    var darkColor = cossacks3._shadeColor(aColor, -0.8);
    var div = document.createElement("div");
    var template = "<div class='Bar' " +
                        "style='width: " + (aMinValue * aScale) + "px; " + "background-color: " + darkColor + ";" + cossacks3._barDarkBorderStyle(aColor) + "'>" +
                    "</div>" +
                    "<div class='Bar' " +
                        "style='width: " + ((aMaxValue - aMinValue) * aScale) + "px; " + cossacks3._barColorStyle(aColor) + cossacks3._barBorderStyle(aColor) + "'>" +
                    "</div>" +
                    "<div class='BarValue'>" + aMaxValue + "</div>";
    div.innerHTML = template;
    return div;
};

cossacks3._barColorStyle = function(aColor) {
    return "background: " + cossacks3.gradient(aColor) + ";";
};

cossacks3.gradient = function(aColor) {
    var secondColor = cossacks3._shadeColor(aColor, -0.5);
    return "linear-gradient(" + aColor + ", " + secondColor + ")";
};

cossacks3.flag = function(aColor) {
    var secondColor = cossacks3._shadeColor(aColor, -0.3);
    return `linear-gradient(to right, ${aColor}, ${secondColor}, ${aColor})`;
};

cossacks3._barBorderStyle = function(aColor) {
    var secondColor = cossacks3._shadeColor(aColor, -0.5);
    return "border: solid 1px " + secondColor + ";";
};

cossacks3._barDarkBorderStyle = function(aColor) {
    var secondColor = cossacks3._shadeColor(aColor, -0.5);
    return "border: solid 1px " + secondColor + ";border-right-width: 0;";
};

cossacks3._shadeColor = function(color, percent) {
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

cossacks3._createBarImage = function(aImagePath) {
    return "<img src='" + aImagePath + "' class='BarImage'>";
};
