"use strict";
wglb.prepareNs("cossacks3");

cossacks3.createUnitImage = function(aUnitId) {
    var size = 30;
    var coords = cossacks3._imagesCoords[aUnitId];

    var canvas = document.createElement("canvas");
    canvas.width = size;
    canvas.height = size;
    canvas.className = "UnitImage";

    var image = new Image(),
    //canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d');

    image.src = 'images/iconsunits.jpg';

    image.onload = function() {
        ctx.drawImage(image,
            coords[0], coords[1],   // source start coord
            46, 46,   // source size
            0, 0,     // Place the result at 0, 0 in the canvas,
            size, size); // scale to
    };
    return canvas;
};

// generated from data\hud\hud.mat
cossacks3._imagesCoords = ﻿{
   "peaaus": [
      0,
      0
   ],
   "peaeng": [
      46,
      0
   ],
   "peaspa": [
      92,
      0
   ],
   "pearus": [
      138,
      0
   ],
   "peaukr": [
      184,
      0
   ],
   "peapol": [
      230,
      0
   ],
   "peatur": [
      276,
      0
   ],
   "peasco": [
      322,
      0
   ],
   "empty": [
      460,
      0
   ],
   "pikeman": [
      0,
      46
   ],
   "pikemanpol": [
      46,
      46
   ],
   "pikemanrus": [
      92,
      46
   ],
   "pikemantur": [
      138,
      46
   ],
   "roundshier": [
      184,
      46
   ],
   "lightinfantry": [
      230,
      46
   ],
   "pikeman18": [
      276,
      46
   ],
   "pikeman18swe": [
      322,
      46
   ],
   "musketeer18bav": [
      368,
      46
   ],
   "grenadierbav": [
      414,
      46
   ],
   "musketeersco": [
      460,
      46
   ],
   "pikemanspa": [
      506,
      46
   ],
   "pikemanpor": [
      552,
      46
   ],
   "pikemanswi": [
      598,
      46
   ],
   "musketeer": [
      0,
      92
   ],
   "musketeerpol": [
      46,
      92
   ],
   "strelet": [
      92,
      92
   ],
   "musketeeraus": [
      138,
      92
   ],
   "musketeerspa": [
      184,
      92
   ],
   "serdiuk": [
      230,
      92
   ],
   "jannisary": [
      276,
      92
   ],
   "archer": [
      322,
      92
   ],
   "archertur": [
      368,
      92
   ],
   "musketeernet": [
      414,
      92
   ],
   "archersco": [
      460,
      92
   ],
   "gauduk": [
      506,
      92
   ],
   "musketeer18": [
      0,
      138
   ],
   "pandur": [
      46,
      138
   ],
   "chasseur": [
      92,
      138
   ],
   "highlander": [
      138,
      138
   ],
   "grenadier": [
      184,
      138
   ],
   "musketeer18pru": [
      230,
      138
   ],
   "musketeer18den": [
      276,
      138
   ],
   "grenadierden": [
      322,
      138
   ],
   "musketeer18sax": [
      368,
      138
   ],
   "grenadiersax": [
      414,
      138
   ],
   "grenadierpru": [
      460,
      138
   ],
   "jagerpor": [
      506,
      138
   ],
   "pandurhun": [
      552,
      138
   ],
   "grenadierhun": [
      598,
      138
   ],
   "jagerswi": [
      644,
      138
   ],
   "officer": [
      0,
      184
   ],
   "officerrus": [
      46,
      184
   ],
   "officertur": [
      92,
      184
   ],
   "officer18": [
      138,
      184
   ],
   "drummer": [
      184,
      184
   ],
   "drummerrus": [
      230,
      184
   ],
   "drummertur": [
      276,
      184
   ],
   "bagpiper": [
      322,
      184
   ],
   "drummer18": [
      368,
      184
   ],
   "officersco": [
      414,
      184
   ],
   "priest": [
      0,
      230
   ],
   "pope": [
      46,
      230
   ],
   "mullah": [
      92,
      230
   ],
   "cannon": [
      138,
      230
   ],
   "howitzer": [
      184,
      230
   ],
   "mortar": [
      230,
      230
   ],
   "multicannon": [
      276,
      230
   ],
   "misdonkey": [
      322,
      230
   ],
   "misflagman": [
      368,
      230
   ],
   "misgeneral": [
      414,
      230
   ],
   "mistrader": [
      460,
      230
   ],
   "padre": [
      506,
      230
   ],
   "croat": [
      0,
      276
   ],
   "wingedhussar": [
      46,
      276
   ],
   "cossacksich": [
      92,
      276
   ],
   "hussar": [
      138,
      276
   ],
   "hussarpru": [
      184,
      276
   ],
   "hackapell": [
      230,
      276
   ],
   "raidersco": [
      276,
      276
   ],
   "lancersco": [
      322,
      276
   ],
   "hussarhun": [
      368,
      276
   ],
   "hussarswi": [
      414,
      276
   ],
   "reiter": [
      0,
      322
   ],
   "reiterswe": [
      46,
      322
   ],
   "vityaz": [
      92,
      322
   ],
   "cossackdon": [
      138,
      322
   ],
   "reiterpol": [
      184,
      322
   ],
   "cossackregister": [
      230,
      322
   ],
   "hetman": [
      276,
      322
   ],
   "spakh": [
      322,
      322
   ],
   "mameluke": [
      368,
      322
   ],
   "cuirassier": [
      414,
      322
   ],
   "guardcavalrysax": [
      460,
      322
   ],
   "sipahi": [
      506,
      322
   ],
   "dragoon": [
      0,
      368
   ],
   "kingmusketeer": [
      46,
      368
   ],
   "tatar": [
      92,
      368
   ],
   "dragoon18": [
      138,
      368
   ],
   "dragoon18fra": [
      184,
      368
   ],
   "lightcavalry.nocolor": [
      230,
      368
   ],
   "lightcavalry": [
      276,
      368
   ],
   "dragoonpol": [
      322,
      368
   ],
   "dragoon18net": [
      368,
      368
   ],
   "dragoon18pie": [
      414,
      368
   ],
   "lightinfantrydip": [
      0,
      414
   ],
   "roundshierdip": [
      46,
      414
   ],
   "archerdip": [
      92,
      414
   ],
   "grenadierdip": [
      138,
      414
   ],
   "cossacksichdip": [
      184,
      414
   ],
   "dragoon18dip": [
      230,
      414
   ],
   "lightcavalrydip": [
      276,
      414
   ],
   "archerturdip": [
      322,
      414
   ],
   "archerscodip": [
      368,
      414
   ],
   "pikemansco": [
      414,
      414
   ],
   "swordsmansco": [
      460,
      414
   ],
   "fishboat": [
      0,
      460
   ],
   "ferry": [
      46,
      460
   ],
   "galley": [
      92,
      460
   ],
   "yacht": [
      138,
      460
   ],
   "yachttur": [
      184,
      460
   ],
   "frigate": [
      230,
      460
   ],
   "xebec": [
      276,
      460
   ],
   "battleship": [
      322,
      460
   ],
   "chaika": [
      368,
      460
   ],
   "framegun": [
      414,
      460
   ]
};
