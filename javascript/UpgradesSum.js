"use strict";

wglb.prepareNs("cossacks3");

cossacks3.createUpgradesSum = function() {
    return {
        attack: { sword: 0, pike: 0, pikePercent: 0, bullet: 0, bulletPercent: 0, bulletPausePercent: 1, arrow: 0 },
        defence: { sword: 0, pike: 0, bullet: 0, cannister: 0, arrow: 0, cannonball: 0 },
        shield: 0,
        buildtimePercent: 0,
        pricepercentage: 0,
        cost: { food: 0, wood: 0, stone: 0, gold: 0, iron: 0, coal: 0 },

        applyUpgrades(aUpgrades) {
            for (let upgrade of aUpgrades) {
                this.applyUpgrade(upgrade);
            }
        },

        applyUpgrade(aUpgrade, factor = 1) {
            switch (aUpgrade.type) {
                case "damage":
                    this.attack.sword += factor * (aUpgrade.sword || 0);
                    this.attack.pike += factor * (aUpgrade.pike || 0);
                    this.attack.bullet += factor * (aUpgrade.bullet || 0);
                    this.attack.arrow += factor * (aUpgrade.arrow || 0);
                    break;
                case "damagepercent":
                    this.attack.bulletPercent += factor * (aUpgrade.bullet || 0);
                    this.attack.pikePercent += factor * (aUpgrade.pike || 0);
                    break;
                case "attackpause":
                    const bonus = 1 + aUpgrade.bullet/100;
                    if (factor === 1) {
                        this.attack.bulletPausePercent *= bonus;
                    } else if (factor === -1) {
                        this.attack.bulletPausePercent /= bonus;
                    } else {
                        throw "nobody expect not -1 nor 1";
                    }
                    break;
                case "buildtime":
                    this.buildtimePercent += factor * aUpgrade.value;
                    break;
                case "shield":
                    this.shield += factor * aUpgrade.value;
                    break;
                case "pricepercentage":
                    this.pricepercentage += factor * 50;//hardcoded for now - ther's only 1 up - 50%
                    break;
                case "defence":
                    this.defence.sword += factor * (aUpgrade.sword || 0);
                    this.defence.pike += factor * (aUpgrade.pike || 0);
                    this.defence.bullet += factor * (aUpgrade.bullet || 0);
                    this.defence.cannister += factor * (aUpgrade.cannister || 0);
                    this.defence.arrow += factor * (aUpgrade.arrow || 0);
                    this.defence.cannonball += factor * (aUpgrade.cannonball || 0);
                    break;
                default:
                    throw "upgrade.type unknown";
            }

            this.cost.food += factor * (aUpgrade.cost.food || 0);
            this.cost.wood += factor * (aUpgrade.cost.wood || 0);
            this.cost.stone += factor * (aUpgrade.cost.stone || 0);
            this.cost.gold += factor * (aUpgrade.cost.gold || 0);
            this.cost.iron += factor * (aUpgrade.cost.iron || 0);
            this.cost.coal += factor * (aUpgrade.cost.coal || 0);
        },

        removeUpgrades(aUpgrades) {
            for (let upgrade of aUpgrades) {
                this.removeUpgrade(upgrade);
            }
        },

        removeUpgrade(aUpgrade) {
            this.applyUpgrade(aUpgrade, -1);
        }
    }
};
